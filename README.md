# BCMR (Bitcoin Cash Metadata Registry)

Basic library for BCMR related data.


## Install

`pip install bcmr`


## Requirements

- `python >= 3.10`
- `requirements.txt` (`pip install -r requirements.txt`)


## Demo

`python demo.py`

You should see something like this:

```
...
```

## Build

`python -m build`


## Publish

Make sure the version in `pyproject.toml` is updated, and the project has been built.

`twine upload --skip-existing dist/*`
