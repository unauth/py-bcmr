#!/usr/bin/env python3

# Library imports
import requests


known_example_filenames = [
    'art-collection.json',
    'decentralized-application.json',
    'fungible-token.json',
    'payouts-or-dividends.json',
]


def fetch_example_content(filename: str) -> str:
    url = f'https://github.com/bitjson/chip-bcmr/raw/master/examples/{filename}'
    response = requests.get(url)
    return response.text


def download_latest():
    for filename in known_example_filenames:
        print(f'downloading {filename}')
        content = fetch_example_content(filename)
        with open(filename, 'w') as f:
            f.write(f'{content.strip()}\n')


if __name__ == '__main__':
    download_latest()
